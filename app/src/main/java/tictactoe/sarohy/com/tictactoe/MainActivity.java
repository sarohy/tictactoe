package tictactoe.sarohy.com.tictactoe;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import java.io.IOException;


public class MainActivity extends AppCompatActivity {

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button btnGame= (Button) findViewById(R.id.btn_play);
        Button btnEnd= (Button) findViewById(R.id.btn_back);
        Typeface custom_font = Typeface.createFromAsset(getAssets(),  "fonts/kirbyss.ttf");
        btnGame.setTypeface(custom_font);
        btnEnd.setTypeface(custom_font);
        btnEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        btnGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplication(),TicTacToeActivity.class));
            }
        });
        getScoreCard();
    }

    @Override
    protected void onResume() {
        getScoreCard();
        super.onResume();
    }

    public void getScoreCard()
    {
        SharedPreferences sharedPref;
        int wins,loses,draws;
        TextView tvWin,tvLose,tvDraw;
        tvWin= (TextView) findViewById(R.id.tv_win);
        tvDraw= (TextView) findViewById(R.id.tv_draw);
        tvLose= (TextView) findViewById(R.id.tv_lose);
        sharedPref = this.getSharedPreferences("Score",Context.MODE_PRIVATE);
        wins = sharedPref.getInt("wins", 0);
        loses = sharedPref.getInt("loses", 0);
        draws = sharedPref.getInt("draws", 0);
        Typeface custom_font = Typeface.createFromAsset(getAssets(),  "fonts/kirbyss.ttf");
        tvWin.setTypeface(custom_font);
        tvLose.setTypeface(custom_font);
        tvDraw.setTypeface(custom_font);
        tvDraw.setText(String.valueOf(draws));
        tvLose.setText(String.valueOf(loses));
        tvWin.setText(String.valueOf(wins));
    }
}
