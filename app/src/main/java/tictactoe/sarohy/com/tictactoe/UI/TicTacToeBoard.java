package tictactoe.sarohy.com.tictactoe.UI;


import android.graphics.Canvas;

import tictactoe.sarohy.com.tictactoe.engine.GameOverException;
import tictactoe.sarohy.com.tictactoe.engine.TicTacToeEngine;
import tictactoe.sarohy.com.tictactoe.engine.WinnerInfo;

public class TicTacToeBoard {
	
	private final int size = 4;
	private int width;
	private int height;
	
	private Cell [] cells;
	
	private TicTacToeEngine engine;
	
	
	public TicTacToeBoard(){		
		engine = new TicTacToeEngine(size);
		cells = new Cell[getCompleteSize()];		
		init();
	}

	private int getCompleteSize() {
		return engine.getCompleteSize();
	}
	
	public void setDimensions(int w,int h){
		width = w;
		height = h;
		
		for(int i=0; i < getCompleteSize(); i++){
			cells[i].setDimensions(getCellWidth(), getCellHeight());
		}
	}
	
	private int getCellWidth(){
		return width / size;
	}
	
	private int getCellHeight(){
		return height / size;
	}
	
	private void init(){
				
		for(int i=0; i < size; i++){
			for (int j=0; j < size; j++){
				cells[cellNum(i,j)] = new Cell(i,j);
			}
		}
	}
	
	private int cellNum(int i,int j){
		return engine.cellNum(i, j);
	}
	
	public void draw(Canvas canvas){
		for(int i=0; i < getCompleteSize(); i++){
			cells[i].draw(canvas);
		}
	}
	
	private Cell getCell(int i,int j){
		return cells[cellNum(i,j)];
	}
	
	private Cell getCell(int num){
		return cells[num];
	}
	
	public boolean update(float x,float y,int turn) {
		int i = (int) x / getCellWidth();
		int j = (int) y / getCellHeight();
		if (engine.checkFree(i,j))
		getCell(i,j).changeState(turn);
		engine.set(i, j, turn);
		return engine.check(i,j,turn);
	}
	public boolean checkFree(float x,float y) {
		int i = (int) x / getCellWidth();
		int j = (int) y / getCellHeight();
		if (engine.checkFree(i,j)){
			return true;
		}
		{
			return false;
		}

	}
	
	public boolean autoUpdate(int turn) {
		try{
			int ind = engine.nextMove(turn);
				
			if(ind >= 0 && ind < cells.length){
				getCell(ind).changeState(turn);	
				engine.set(ind, turn);
				return engine.check(ind,turn);
			}
		}
		catch(GameOverException ex){

		}
		
		return false;		
	}
	
	
	public void drawOnOver(Canvas canvas){
		
		if (!isGameDraw()) {		
			WinnerInfo info = engine.getWinnerInfo();
			int [] cellNums = info.getCells();
				
			for(int i=0; i < cellNums.length; i++){		
				cells[cellNums[i]].setWinState(info.getPlayer());
			}
		}
		
		draw(canvas);		
	}
	
	public boolean isGameDraw(){
		return engine.getWinnerInfo().isDraw();
	}
	public int whichWinner(){
		WinnerInfo winnerInfo=engine.getWinnerInfo();
		return winnerInfo.getPlayer();
	}

}
