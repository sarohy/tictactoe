package tictactoe.sarohy.com.tictactoe.UI;

import android.graphics.Canvas;

public class Cell {
	
	
	private int x;
	private int y;
	private int width;
	private int height;
	private int player;
	private boolean empty;
	private CellState state;
	
	
	public Cell(int x,int y){
		
		this.x = x;
		this.y = y;
		player = -1;
		
		state = new CellState();
		empty = true;
	}
	
	public void setDimensions(int w,int h){
		width = w;
		height = h;
	}
	
	public void draw(Canvas canvas){
		state.draw(canvas, x, y, width, height);
	}
	
	public void changeState(int turn){
		state = new CellCheckedState(turn);
		empty = false;
	}

	public void setWinState(int player){
		state = new CellWinState(player);		
	}
	
	

}


