package tictactoe.sarohy.com.tictactoe;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;

import java.io.IOException;

import tictactoe.sarohy.com.tictactoe.UI.GameView;

public class TicTacToeActivity extends Activity
{
	GameView.GameViewActionListener listener;
    SharedPreferences sharedPref;
    int wins,loses,draws;
    /** Called when the activity is first created. */
    String name="Sarohy";
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        createListener();        
        drawBoard();
        Button btnGame= (Button) findViewById(R.id.button_new);
        Typeface custom_font = Typeface.createFromAsset(getAssets(),  "fonts/kirbyss.ttf");
        btnGame.setTypeface(custom_font);
        sharedPref = this.getSharedPreferences("Score",Context.MODE_PRIVATE);
        wins = sharedPref.getInt("wins", 0);
        loses = sharedPref.getInt("loses", 0);
        draws = sharedPref.getInt("draws", 0);
    }
    
    private void createListener(){
    	 listener = new GameView.GameViewActionListener() {
 			
 			@Override
 			public void isOver(String message) {
                setMessage(message);
 			}

             @Override
             public void setTurn(int turn) {
                 setUpTurn(turn);
             }
         };
    }
    public void setUpTurn(int turn) {
        TextView pText = findViewById(R.id.tv_player);
        TextView cText = findViewById(R.id.tv_comp);
        if (turn == 0) {
            cText.setBackgroundColor(Color.TRANSPARENT);
            pText.setBackgroundColor(Color.RED);
        }
        else {
            pText.setBackgroundColor(Color.TRANSPARENT);
            cText.setBackgroundColor(Color.BLUE);
        }
    }
    private void drawBoard(){
    	ViewGroup view = (ViewGroup) findViewById(R.id.game);
    	
    	View gameView = new GameView(this,null,listener);    	    	    	
    	view.addView(gameView);
    }
    
    private void clear(){
    	ViewGroup view = (ViewGroup) findViewById(R.id.game);
    	view.removeAllViews();
    }
    
    public void newGame(View view){
    	clear();
    	drawBoard();
    }
    
    public void setMessage(String message){


        final Dialog dialog=new Dialog(this);
        dialog.setContentView(R.layout.dialog);
        TextView textView=(TextView) dialog.findViewById(R.id.tv_detail);
        Typeface custom_font = Typeface.createFromAsset(getAssets(), "fonts/kirbyss.ttf");
        textView.setTypeface(custom_font);
        textView.setText(message);
        Button btnReplay=(Button) dialog.findViewById(R.id.btn_replay);
        btnReplay.setTypeface(custom_font);
        btnReplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                clear();
                drawBoard();
            }
        });
        Button btnBack=(Button) dialog.findViewById(R.id.btn_back);
        btnBack.setTypeface(custom_font);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                finish();
            }
        });
        dialog.show();
        if (message.toLowerCase().contains("Game Draw".toLowerCase())){
            draws++;
        }
        else if (message.toLowerCase().contains("Computer".toLowerCase())){
            loses++;
        }
        else if (message.toLowerCase().contains("Player".toLowerCase())){
            wins++;
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            final EditText edittext = new EditText(getApplicationContext());
            alert.setMessage("");
            alert.setTitle("Upload score to server");

            alert.setView(edittext);
            edittext.setHint("Enter your name.....");
            alert.setPositiveButton("Upload", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    //What ever you want to do with the value
                    Editable YouEditTextValue = edittext.getText();
                    //OR
                    name = edittext.getText().toString();
                    new UploadScore().execute();
                    dialog.dismiss();
                }
            });
            alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    // what ever you want to do with No option.
                    dialog.dismiss();
                }
            });
            alert.show();
        }
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt("wins",wins);
        editor.putInt("loses",loses);
        editor.putInt("draws",draws);
        editor.commit();
    }
    class UploadScore extends AsyncTask<String, Void, Void> {

        private Exception exception;

        protected Void doInBackground(String... urls) {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("https://darwin.bournemouth.ac.uk//prog2/scores.php?key=prog2resit");

            try {
                StringEntity se = new StringEntity( "<score name=\'"+name+"\'>"+wins+"</score>", HTTP.UTF_8);
                se.setContentType("text/xml");
                httppost.setEntity(se);

                HttpResponse httpresponse = httpclient.execute(httppost);
                HttpEntity resEntity = httpresponse.getEntity();
            }
            catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(Void feed) {
        }
    }
}
