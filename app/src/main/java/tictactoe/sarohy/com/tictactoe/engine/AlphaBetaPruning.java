package tictactoe.sarohy.com.tictactoe.engine;

public class AlphaBetaPruning {
	
	final int NegativeInfinity = Integer.MIN_VALUE;
	final int Infinity = Integer.MAX_VALUE;

	public AlphaBetaPruning(){

	}
	/*It is an artificial intelligence algorithm
	Name as Minimax-Alpha-Beta-Puring
	This algo is widely used in one to one games
	This algo is used for computer to think what to play next.
	In this we check how much one mark can benefit us.
	* */
	int alphabeta(AlphaBetaNode node,int depth){
		return alphabetaMax(node, depth, NegativeInfinity, Infinity);
	}

	//this function will return max profit of its child node. It is mostly used by payer for whom we want to maximize profit of
	// making move.
	private int alphabetaMax(AlphaBetaNode node, int depth, int alpha, int beta) {
		if (depth==0)
			return node.evaluate();
		int value=NegativeInfinity;
		depth--;
		for (Action action : node.actions() ) {
			value = Math.max(value, alphabetaMin(action.getNode(),depth,alpha,beta));
			alpha = Math.max(value, alpha);
			if(value >= beta) return value;
			action.setValue(value);

		}
		return value;
	}
	//This will return minimum profit from opposite player.
	private int alphabetaMin(AlphaBetaNode node, int depth, int alpha, int beta) {
		if (depth==0)
			return node.evaluate();
		int value = Infinity;
		depth--;
		for (Action action : node.actions() ) {
			value = Math.min(value, alphabetaMax(action.getNode(),depth,alpha,beta));
			beta = Math.min(value, beta);
			if(value <= alpha) return value;
			action.setValue(value);

		}
		return value;
	}
}
