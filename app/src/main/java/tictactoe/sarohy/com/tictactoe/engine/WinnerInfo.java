package tictactoe.sarohy.com.tictactoe.engine;


public class WinnerInfo {
	
	private int player;
	private int [] cells;
	
	public WinnerInfo(int p, int [] c){
		this.player = p;
		cells = c;
	}
	
	public int getPlayer(){
		return player;
	}
	
	public int[] getCells(){
		return cells;
	}
	
	public boolean isDraw(){
		return player == -1;
	}
	

}
