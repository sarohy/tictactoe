package tictactoe.sarohy.com.tictactoe.engine;

public class Action {
	
	private AlphaBetaNode node;
	private int actionIndex;
	private int value;
	
	public Action(AlphaBetaNode node){
		this.node = node;
	}
	
	public void setValue(int value){
		this.value = value;
	}
	
	public int getValue(){
		return value;
	}
	
	public void setActionIndex(int value){
		actionIndex = value;
	}
	
	public int getActionIndex(){
		return actionIndex;
	}
	
	public AlphaBetaNode getNode(){
		return node;
	}

}
