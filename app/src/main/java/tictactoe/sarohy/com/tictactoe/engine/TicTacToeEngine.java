package tictactoe.sarohy.com.tictactoe.engine;

import java.util.Date;
import java.util.Random;
import java.util.Vector;

public class TicTacToeEngine implements AlphaBetaNode{
	
	private int size;
	private int [][] state;
	private int freeCellsNumber;
	private int currentTurn;
	
	Vector<Action> actions;
	
	
	WinnerInfo info;
	
	
	public TicTacToeEngine(int size){
		this.size = size;
		state = new int[size][size];
		
		for(int i=0; i < size; i++){
			for(int j=0; j < size; j++){
				state[i][j] = -1;
			}
		}
		
		freeCellsNumber = size * size;
	}
	
	public int getCompleteSize(){
		return size*size;
	}
	//return deep copy
	public TicTacToeEngine clone(){
		TicTacToeEngine newEngine = new TicTacToeEngine(size);
		
		for(int i=0; i < size; i++){
			for(int j=0; j < size; j++){
				newEngine.state[i][j] = this.state[i][j];
			}
		}
		
		newEngine.freeCellsNumber = this.freeCellsNumber;
		newEngine.currentTurn = this.currentTurn;
		
		return newEngine;
	}
	/*
	Mapping is from top to down and then left to right for each cell
	* */
	public void set(int i,int t){
		set(getI(i),getJ(i),t);
	}
	
	public boolean check(int i,int t){
		return check(getI(i),getJ(i),t);
	}
	
	private int getI(int index){
		return index / size;
	}
	
	private int getJ(int index){
		return index % size;
	}

	//setting on location with turn 0 or 1
	public void set(int i,int j,int t){
		if(state[i][j] == -1){			
			state[i][j] = t;
			freeCellsNumber--;		
			currentTurn = t;
		}
	}
	//checking weather that i j location is filled or not
	public boolean checkFree(int i,int j){
		if(state[i][j] == -1){
			return true;
		}
		return false;
	}
	//Checking for game over
	public boolean check(int row,int col,int turn) {
		boolean result = true;
						
		result = horizontal(row,turn) || vertical(col,turn) || leftDiagonal(turn) || rightDiagonal(turn);
		
		if ( !result && isDraw() ){
			result = isDraw();
			createWinnerInfo(result,-1,null);
		}
				
		return result;		
	}
	//Checking for horizontal
	private boolean horizontal(int row, int turn){
		
		boolean result = true;
		int [] cells = new int[size];
		
		for(int j=0; j < size; j++){
			if (state[row][j] != turn){
				result = false;				
			}
			cells[j] = row*size + j;
		}
		
		createWinnerInfo(result,turn,cells);
		return result;
		
	}
	//checking for vertical
	private boolean vertical(int col, int turn){
		
		boolean result = true;
		int [] cells = new int[size];
		
		for(int i=0; i < size; i++){
			if (state[i][col] != turn){
				result = false;				
			}
			cells[i] = i*size + col;
		}
		
		createWinnerInfo(result,turn,cells);
		return result;
		
	}
	//checking for diagonal
	private boolean leftDiagonal(int turn){
		
		boolean result = true;
		int [] cells = new int[size];
				
		for(int i=0; i < size; i++){
			if (state[i][i] != turn){
				result = false;				
			}
			cells[i] = i*size + i;
		}
		
		createWinnerInfo(result,turn,cells);
		return result;
		
	}
	//checking for diagonal
	private boolean rightDiagonal(int turn){
		
		boolean result = true;
		int [] cells = new int[size];
				
		for(int i=0; i < size; i++){
			if (state[i][size-1-i] != turn){
				result = false;				
			}
			cells[i] = i*size + (size-1-i);
		}
		
		createWinnerInfo(result,turn,cells);
		return result;
		
	}
	//checking wether game is drawn or not
	public boolean isDraw(){
		return freeCellsNumber == 0;
	}
	//Setting which player won match
	public void createWinnerInfo(boolean result,int turn,int [] cells){
		if(result){
			info = new WinnerInfo(turn,cells);
		}
	}
	
	public WinnerInfo getWinnerInfo(){
		return info;
	}
	//checking if the turn person win or not
	public boolean completeCheck(int turn){
		boolean result = false;
		
		for (int i=0; i < size; i++){
			result = result || horizontal(i,turn);
			result = result || vertical(i, turn);
		}
		
		result = result || leftDiagonal(turn) || rightDiagonal(turn);				
		
		return result;
	}
    //estimating profit for Alpha-Beta pruning
	@Override
	public int evaluate() {
		int value = 0;
		if(completeCheck(1)){
			value = 1;
		}
		else if (completeCheck(0)){
			value = -1;
		}
		else value = 0;
		
		return value;
		
	}
    //getting all possible moves available on board
	@Override
	public Vector<Action> actions() {
		
		actions = new Vector<Action>();
		
		Vector<Integer> freeCellsIndex = findFreeCells();
		for(Integer index : freeCellsIndex){
			TicTacToeEngine child = this.clone();			
			child.set(index, child.nextTurn());
			Action action = new Action(child);
			action.setActionIndex(index);
			actions.add(action);			
		}
		
		return actions;
	}
	//checking if that i,j is free cell or not
	private boolean isFree(int i,int j){
		return state[i][j] == -1;
	}

	public int cellNum(int i,int j){
		return (i*size)+j;
	}

	//list of free cell available
	public Vector<Integer> findFreeCells(){
		Vector<Integer> index = new Vector<Integer>();
		
		for(int i=0; i < size; i++){
			for(int j=0; j < size; j++){
				if(isFree(i,j)){
					index.add(cellNum(i,j));
				}
			}
		}
		
		return index;
	}
	//getting computer player move
	public int nextMove(int turn) throws GameOverException {
		int pos= nextMoveAlphaBeta();
		set(getI(pos),getJ(pos),turn);
		return pos;
	}
	//get a random move
	private int nextMoveRandom() throws GameOverException {
		Vector<Integer> freeCellsIndex = findFreeCells();
		if (freeCellsIndex.size() > 0){
			return freeCellsIndex.get(random(freeCellsIndex.size() - 1));
		}
		else throw new GameOverException();
	}
    //get move by purring
	private int nextMoveAlphaBeta() throws GameOverException {
		if (isDraw()) throw new GameOverException();
		
		AlphaBetaPruning prune = new AlphaBetaPruning();

		if (freeCellsNumber==1)
		{
			Vector<Integer> freeCellsIndex = findFreeCells();
			freeCellsIndex.get(0);
			return freeCellsIndex.get(0);
		}
		else {
			int value;
			if (nextTurn() == 1) {
				value = prune.alphabeta(this, 2);
			} else {
				value = prune.alphabeta(this, 2);
			}
			for (Action action : actions) {
				if (action.getValue() == value) {
					return action.getActionIndex();
				}
			}
		}
		return nextMoveRandom();
	}
	//Switching of player
	private int nextTurn(){
		return (currentTurn+1) % 2;
	}
	
	//random generator
	private int random(int limit){
		Random rand = new Random( new Date().getTime() );
        int d=-1;
        while (d<0){
            d=(int)rand.nextInt(limit);
        }
		return d;
	}

}
