package tictactoe.sarohy.com.tictactoe.UI;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Paint.Style;

public class CellState {
	
	protected Paint paint;
	
	public CellState(){
		paint = new Paint();
		paint.setStyle(Style.STROKE);
	}
	
	public void draw(Canvas canvas,int x,int y,int width,int height){
		paint.setStrokeWidth(4);
        paint.setColor(Color.WHITE);
		Rect rect = new Rect(x*width,y*height,((x+1)*width - 1),((y+1)*height - 1));
        canvas.drawRect(rect, paint);
	}

}
