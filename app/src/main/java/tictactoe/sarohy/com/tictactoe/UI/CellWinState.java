package tictactoe.sarohy.com.tictactoe.UI;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.Paint.Style;

public class CellWinState extends CellCheckedState {

	int winPlayer;
	public CellWinState(int t) {
		super(t);
		winPlayer=t;
	}
	public void draw(Canvas canvas,int x,int y,int width,int height){

		if (winPlayer==1) {
			paint.setStyle(Style.FILL);
			paint.setAlpha(10);
			paint.setColor(Color.rgb(2, 249, 245));

			Rect rect = new Rect(x * width - 1, y * height - 1, ((x + 1) * width - 2), ((y + 1) * height - 2));
			canvas.drawRect(rect, paint);

			paint.setStyle(Style.STROKE);
			paint.setColor(Color.BLACK);
		}
		else {
			paint.setStyle(Style.FILL);
			paint.setAlpha(10);
			paint.setColor(Color.rgb(209, 148, 148));

			Rect rect = new Rect(x * width - 1, y * height - 1, ((x + 1) * width - 2), ((y + 1) * height - 2));
			canvas.drawRect(rect, paint);

			paint.setStyle(Style.STROKE);
			paint.setColor(Color.BLACK);
		}
		super.draw(canvas, x, y, width, height);
	}

}
