package tictactoe.sarohy.com.tictactoe.UI;




import android.content.Context;
import android.graphics.Canvas;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import tictactoe.sarohy.com.tictactoe.R;


public class GameView extends View {
	
	TicTacToeBoard board;
	int turn;	
	boolean isOver;
	private GameViewActionListener listener;
	
  
	public GameView(Context context, AttributeSet attrs, GameViewActionListener listener) {
		super(context, attrs);
		board = new TicTacToeBoard();
		turn = 0;
		listener.setTurn(turn);
		isOver = false;
		this.listener = listener;
	}
	private String getGameOverMessage(){
		String message = "";
		if(board.isGameDraw()){
			message ="Game Draw\nStart a new game";
		}
		else {
			int p=board.whichWinner();
			if (p==0)
				message = "Game Over!!\nCongrats!!\nPlayer won";
			else
				message = "Game Over!!\nOpps!!\nComputer won\nTry Again";
		}
		return message;
	}
	
	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
	    super.onMeasure(widthMeasureSpec, heightMeasureSpec);
	    int size = 0;
	    int width = getMeasuredWidth();
	    int height = getMeasuredHeight();

	    if (width > height) {
	        size = height;
	    } else {
	        size = width;
	    }
	    setMeasuredDimension(size, size);
	}
	

	@Override
	protected void onDraw(Canvas canvas) {		
		board.setDimensions(this.getWidth(), this.getHeight());
		board.draw(canvas);
		if (isOver){
			listener.isOver(getGameOverMessage());
			board.drawOnOver(canvas);
		}
		else {			
			systemTurn();
		}

	}
	
	public boolean onTouchEvent(MotionEvent event){
		float x = event.getX();
		float y = event.getY();

		switch (event.getAction()) {
	    	case MotionEvent.ACTION_DOWN:	    		
	    		return true;	      
	    	case MotionEvent.ACTION_UP:
	    		playerTurn(x, y);
	    		break;
	    	default:
	    		return false;
	    }


		return true;
	}

	private void playerTurn(float x, float y) {
		if(!isOver && (turn % 2 == 0) && board.checkFree(x,y) ){
			isOver = board.update(x,y,turn);
			turn = (turn + 1) % 2;
			invalidate();
		}
	}
	
	private void systemTurn(){
		listener.setTurn(turn);
		if(!isOver && (turn % 2 == 1) ){
			Handler handler = new Handler();
			handler.postDelayed(new Runnable(){
				public void run(){			
					
					isOver = board.autoUpdate(turn);
					turn = (turn + 1) % 2;
					invalidate();					
					listener.setTurn(turn);
				}
			},1000);


		}
		
	}
	
	public interface GameViewActionListener {
		public void isOver(String message);

		public void setTurn(int turn);
	}

} 
