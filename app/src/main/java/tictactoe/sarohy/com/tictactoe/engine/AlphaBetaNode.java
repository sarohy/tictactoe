package tictactoe.sarohy.com.tictactoe.engine;

import java.util.Vector;

public interface AlphaBetaNode {
	
	int evaluate();
	Vector<Action> actions();

}
