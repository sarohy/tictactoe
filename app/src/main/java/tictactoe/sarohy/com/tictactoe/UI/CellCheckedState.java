package tictactoe.sarohy.com.tictactoe.UI;

import android.graphics.Canvas;
import android.graphics.Color;

public class CellCheckedState extends CellState {

	int turn;
	
	public CellCheckedState(int t){
		this.turn = t;
	}
	
	public void draw(Canvas canvas,int x,int y,int width,int height){
		super.draw(canvas,x,y,width,height);

		paint.setStrokeWidth(8);
		if(turn == 0){
			paint.setColor(Color.RED);
			canvas.drawCircle(x*width + width/2, y*height + height/2, width/4, paint);
		}
		else{
			paint.setColor(Color.BLUE);
			canvas.drawLine(x*width + width/4, y*height + height/4, x*width + width*3/4, y*height + height*3/4, paint);
			canvas.drawLine(x*width + width/4, y*height + height*3/4, x*width + width*3/4, y*height + height/4, paint);
		}
		paint.setStrokeWidth(1);
	}
	
}
